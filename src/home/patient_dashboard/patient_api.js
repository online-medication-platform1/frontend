import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
	patient: "/patient_home/",
};

const accessToken = localStorage.getItem("login") ? 
	JSON.parse(localStorage.getItem("login")).user.type +
	" " +
	JSON.parse(localStorage.getItem("login")).user.token : null;
const id = localStorage.getItem("login") ? JSON.parse(localStorage.getItem("login")).user.id : null;

function getPatient(callback) {
	let request = new Request(HOST.backend_api + endpoint.patient + id, {
		method: "GET",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
	});
	console.log(request.url);
	RestApiClient.performRequest(request, callback);
}

export {getPatient};