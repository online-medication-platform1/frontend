import React from "react";
import { Container, Jumbotron } from "react-bootstrap";
import * as API_USERS from "./patient_api";
import MedicationTable from "./medication_table";
import { Redirect } from "react-router-dom";


class PatientHome extends React.Component{
    constructor(props){
		super(props);
		
		this.getPatient = this.getPatient.bind(this);
		this.parsePatient = this.parsePatient.bind(this);
		this.state = {
			patientData: null,
			isLoaded: false,
			isParsed: false,
			errorStatus: "",
			error: "",
			medicationPlanData: null,
		}
		this.user = localStorage.getItem("login") ? JSON.parse(localStorage.getItem("login")).user : null;
		
	}

	componentDidMount(){
		this.getPatient();
	}

	getPatient(){
		return API_USERS.getPatient((result, status, err) => {
			if (result !== null && status === 200) {
				this.setState({
					patientData: result,
					isLoaded: true,
					errorStatus: null,
					error: null,
				});
				this.parsePatient(this.state.patientData);
			} else {
				console.log(status);
				console.log(err);

				this.setState({
					errorStatus: status,
					error: err,
				});
				if(err.error !== "Forbidden") 
					window.location.reload();
			}
		});
	}
	
	parsePatient(patient){
		let arr = [];
		let parsed = [];
		patient.medicationPlan.medicationsList.forEach(element => {
			arr.push(element.name);
		});

		for(let i=0; i<patient.medicationPlan.medicationsList.length; i++){
			parsed.push({
				medications: arr[i],
				intakeInterval: patient.medicationPlan.intakeInterval[i],
			})
		}

		this.setState({
			medicationPlanData: parsed, 
			isParsed: true,
		})
	}

    render() {
		let isSuccess, hasPatientRole;
		let isLogged = JSON.parse(localStorage.getItem("login"));

		if (isLogged) {
			isSuccess = isLogged.login;
			hasPatientRole = isLogged.user.roles[0] === "ROLE_PATIENT";
		}
		if(!hasPatientRole && isLogged){
			localStorage.removeItem("login");
		}
		return (
			<div>	
				<div>
					{isSuccess && hasPatientRole ? null  : <Redirect to="/signin"/>}
				</div>
			<Jumbotron fluid>
					<Container>
						<h1>Account information</h1>
						<h1>Patient {isLogged.user.name}</h1>
						{this.state.isLoaded && (
							<div>
								<p>Email: {this.state.patientData.email}</p>
								<p>Address: {this.state.patientData.address}</p>
								<p>Birth Data: {this.state.patientData.birthDate}</p>
								<p>Gender: {this.state.patientData.gender}</p>
								<p>Medical Record: {this.state.patientData.medicalRecord}</p>
							</div>)
						}
					</Container>
			</Jumbotron>
			<h2>Medication Plan</h2> 
			{this.state.isLoaded && (<h3>Perioad Tratment: {/* {this.state.medicationPlanData.perioadTreatment} */}</h3>)}
			{this.state.isLoaded && this.state.isParsed && (
				<MedicationTable medicationPlanData={this.state.medicationPlanData}/>
			)}
			</div>
		)
	}
}

export default PatientHome;