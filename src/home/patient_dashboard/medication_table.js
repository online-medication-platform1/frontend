import React from "react";
import Table from "../../commons/tables/table";

const columns = [
	{
		Header: "Medication",
		accessor: "medications",
	},
	{
		Header: "IntakeInterval",
		accessor: "intakeInterval",
	},
];

const filters = [
	{
		accessor: "medications",
	},
];

class MedicationTable extends React.Component {
	constructor(props) {
        super(props);
		this.state = {
			medicationPlanData: this.props.medicationPlanData,
        };
	}

	

	render() {
		return (
			<Table
                data={this.state.medicationPlanData}
				columns={columns}
				search={filters}
				pageSize={5}
				rowSelected={this.props.rowSelected}
				height='200px'
			/>
		);
	}
}

export default MedicationTable;
