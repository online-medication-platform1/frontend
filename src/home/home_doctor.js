import React from "react";
import { Container, Jumbotron } from "react-bootstrap";
import { Redirect } from "react-router-dom";

class DoctorHome extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let isSuccess, hasDoctorRole;
		let isLogged = JSON.parse(localStorage.getItem("login"));

		if (isLogged) {
			isSuccess = isLogged.login;
			hasDoctorRole = isLogged.user.roles[0] === "ROLE_DOCTOR";
		}
		if(!hasDoctorRole && isLogged && isSuccess){
			localStorage.removeItem("login");
		}

		return (
			<div>
				<div>
					{isSuccess && hasDoctorRole ? null  : <Redirect to="/signin"/>}
				</div>

				<Jumbotron fluid>
					<Container>
						<h1>Status: {isLogged.user.name} is authenticated</h1>
						<h1>Doctor HomePage</h1>
						<p>
						Select from the menu to view the tables
						</p>
					</Container>
				</Jumbotron>
			</div>
		);
	}
}

export default DoctorHome;
