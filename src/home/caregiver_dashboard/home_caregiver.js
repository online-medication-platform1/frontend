import { HOST } from "../../commons/hosts";
import React from "react";
import { Container, Jumbotron } from "react-bootstrap";
import * as API_USERS from "./caregiver_api";
import PatientTable from "./patient_table";
import { Redirect } from "react-router-dom";
/* import SockJS from 'sockjs-client';
import Stomp from 'stompjs'; */
import SockJsClient from 'react-stomp';
 


class CaregiverHome extends React.Component{
    constructor(props){
		super(props);
		
		this.getCaregiver = this.getCaregiver.bind(this);
		this.parseCaregiver = this.parseCaregiver.bind(this);
		//this.connect = this.connect.bind(this);
		//this.disconnect = this.disconnect.bind(this);
		//this.stompClient = null;
		this.state = {
			caregiverData: null,
			isLoaded: false,
			isParsed: false,
			errorStatus: "",
			error: "",
			patientData: null,
		}

        this.user = localStorage.getItem("login") ? JSON.parse(localStorage.getItem("login")).user : null;
	}

	componentDidMount(){
		this.getCaregiver();
		//this.connect();
	}

	/* connect(){
		var socket = new SockJS(HOST.backend_api+"/gs-guide-websocket");
		console.log("AICI 1");
		/* this.setState({
			stompClient: Stomp.over(socket)
		}) 
		this.stompClient = Stomp.over(socket);
		console.log("AICI 2");
    	this.stompClient.connect({}, function (frame) {
			console.log('Connected: ' + frame);
		});
		this.stompClient.subscribe('/topic/monitoredData', function (greeting) {
			//showGreeting(JSON.parse(greeting.body).content);
			console.log(JSON.parse(greeting.body).content);
			});
	} 

	disconnect() {
		if (this.state.stompClient !== null) {
			this.state.stompClient.disconnect();
		}
		console.log("Disconnected");
	} */
	
	getCaregiver(){
		API_USERS.getCaregiver((result, status, err) => {
			if (result !== null && status === 200) {
				this.setState({
					caregiverData: result,
					patientData: result.listPersonCareOf,
					isLoaded: true,
				});
				console.log(result.listPersonCareOf);
			} else {
				this.setState({
					errorStatus: status,
					error: err,
				});
				if(err.error !== "Forbidden") 
					window.location.reload();
			}
		});
	}
	
	parseCaregiver(caregiver){
		let arr = [];
		let parsed = [];
		caregiver.patient.medicationsList.forEach(element => {
			arr.push(element.name);
		});

		for(let i=0; i<caregiver.patient.medicationsList.length; i++){
			parsed.push({
				medications: arr[i],
				intakeInterval: caregiver.patient.intakeInterval[i],
			})
		}
		this.setState({
			patientData: parsed, 
			isParsed: true,
		})
	}

    render() {
		let isSuccess, hasCaregiverRole;
		let isLogged = JSON.parse(localStorage.getItem("login"));

		if (isLogged) {
			isSuccess = isLogged.login;
			hasCaregiverRole = isLogged.user.roles[0] === "ROLE_CAREGIVER" ? true : false;
		}
		if(!hasCaregiverRole){
			localStorage.removeItem("login");
		}
		return (
			<div>	
				<div>
					{isSuccess && hasCaregiverRole ? null  : <Redirect to="/signin"/>}
				</div>
				<div>
				<SockJsClient url='https://ds2020-lucian-spring1.herokuapp.com/gs-guide-websocket/'
    				topics={['/topic/monitoredData']}
						onConnect={() => {
							console.log("connected");
						}}
						onDisconnect={() => {
							console.log("Disconnected");
						}}
						onMessage={(msg) => {
							console.log(msg);
						}}
						ref={(client) => {
							this.clientRef = client
						}}/>
				</div>
			<Jumbotron fluid>
					<Container>
						<h1>Account information</h1>
						<h1>Caregiver {isLogged.user.name}</h1>
						{this.state.isLoaded && (
							<div>
								<p>Email: {this.state.caregiverData.email}</p>
								<p>Address: {this.state.caregiverData.address}</p>
								<p>Birth Data: {this.state.caregiverData.birthDate}</p>
								<p>Gender: {this.state.caregiverData.gender}</p>
							</div>)
						}
					</Container>
			</Jumbotron>
			<h2>Patient to take care of:</h2> 
			{this.state.isLoaded && (
				<PatientTable patientData={this.state.patientData}/>
			)}
			</div>
		)
	}
}

export default CaregiverHome;