import React from "react";
import Table from "../../commons/tables/table";

const columns = [
	{
		Header: "ID",
		accessor: "id",
	},
	{
		Header: "Email",
		accessor: "email",
	},
	{
		Header: "Name",
		accessor: "name",
	},
	{
		Header: "Gender",
		accessor: "gender",
	},
	{
		Header: "BirthDate",
		accessor: "birthDate",
	},
	{
		Header: "Address",
		accessor: "address",
	},
	{
		Header: "Role",
		accessor: "role",
	},
];

const filters = [
	{
		accessor: "name",
	},
];

class CaregiverTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: this.props.tableData,
		};
	}

	render() {
		return (
			<Table
				data={this.state.tableData}
				columns={columns}
				search={filters}
				pageSize={10} 
				rowSelected={this.props.rowSelected}
			/>
		);
	}
}

export default CaregiverTable;
