import React from "react";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Col, Row } from "reactstrap";
import { FormGroup, Input, Label } from "reactstrap";

class CaregiverFormUpdate extends React.Component {
	constructor(props) {
		super(props);
		this.reloadHandler = this.props.reloadHandler;
		this.user = this.props.user;
		if(this.user === null)
			this.user = {email: "", password: "", name: "", birthDate: "", gender: "", role: "", id: "", address: "", medicalRecord: ""}

		this.state = {
			errorStatus: 0,
			error: null,

			formIsValid: false,

            userData: {
                name: this.user.name,
                email: this.user.email,
                password: this.user.password,
				birthDate: this.user.birthDate,
				address: this.user.address,
				gender: this.user.gender,
				role: this.user.role,
            },
        };
        this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	updatePatient(Patient) {
		return API_USERS.updateCaregiver(Patient, (result, status, error) => {
			if (result !== null && (status === 200 || status === 201)) {
				console.log("Successfully update Patient with id: " + result);
				this.reloadHandler();
			} else {
				this.setState({
					errorStatus: status,
					error: error,
				});
			}
		});
    }
    
    handleChange = (event) => {
		const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.userData;
        updatedControls[name] = value;
    }

	handleSubmit() {
		let Patient = {
            id: this.user.id,
			email: this.state.userData.email,
			password: this.state.userData.password,
			name: this.state.userData.name,
			birthDate: this.state.userData.birthDate,
			gender: this.state.userData.gender,
			address: this.state.userData.address,
			role: this.state.userData.role,
		};

		console.log(Patient);
		this.updatePatient(Patient);
	}

	render() {
		return (
			<div>
				<FormGroup id="name">
					<Label for="nameField"> Name: </Label>
					<Input
						name="name"
						id="nameField"
                        defaultValue={this.state.userData.name}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="email">
					<Label for="emailField"> Email: </Label>
					<Input
						name="email"
						id="emailField"
                        defaultValue={this.state.userData.email}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="password">
					<Label for="passwordField"> Password: </Label>
					<Input
						name="password"
						id="passwordField"
                        defaultValue={this.state.userData.password}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="birthDate">
					<Label for="birthDateField"> Birthdate: </Label>
					<Input
						name="birthDate"
						id="birthDateField"
                        defaultValue={this.state.userData.birthDate}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="gender">
					<Label for="genderField"> Gender: </Label>
					<Input
						name="gender"
						id="genderField"
                        defaultValue={this.state.userData.gender}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="address">
					<Label for="addressField"> Address: </Label>
					<Input
						name="address"
						id="addressField"
                        defaultValue={this.state.userData.address}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="role">
					<Label for="roleField"> Role: </Label>
					<Input
						name="role"
						id="roleField"
                        defaultValue={this.state.userData.role}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<Row>
					<Col sm={{ size: "4", offset: 8 }}>
						<Button
							type={this.user.name==="" ? "Insert" : "Update"}
							onClick={this.handleSubmit}
						>
							{" "}
							Submit{" "}
						</Button>
					</Col>
				</Row>

				{this.state.errorStatus > 0 && (
					<APIResponseErrorMessage
						errorStatus={this.state.errorStatus}
						error={this.state.error}
					/>
				)}
			</div>
		);
	}
}

export default CaregiverFormUpdate;
