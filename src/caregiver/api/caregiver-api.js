import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
	caregivers: "/doctor/caregivers",
	insert_caregiver: "/doctor/insert_caregiver",
	update_caregiver: "/doctor/update_caregiver",
	remove_caregiver: "/doctor/remove_caregiver",
};

const accessToken = localStorage.getItem("login") ? 
	JSON.parse(localStorage.getItem("login")).user.type +
	" " +
	JSON.parse(localStorage.getItem("login")).user.token : null;

function getCaregivers(callback) {
	let request = new Request(HOST.backend_api + endpoint.caregivers, {
		method: "GET",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
	});
	console.log(request.url);
	RestApiClient.performRequest(request, callback);
}

function postCaregiver(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.insert_caregiver, {
		method: "POST",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function updateCaregiver(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.update_caregiver, {
		method: "PUT",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(id, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.remove_caregiver, {
		method: "DELETE",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(id),
	});

	RestApiClient.performRequest(request, callback);
}

export { getCaregivers, postCaregiver, updateCaregiver, deleteCaregiver };
