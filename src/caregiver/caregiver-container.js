import React from "react";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
	Button,
	Card,
	CardHeader,
	Col,
	Modal,
	ModalBody,
	ModalHeader,
	Row,
} from "reactstrap";
import CaregiverForm from "./components/caregiver-form";
import CaregiverFormUpdate from "./components/caregiver-form-update";

import * as API_USERS from "./api/caregiver-api";
import CaregiverTable from "./components/caregiver-table";

class CaregiverContainer extends React.Component {
	constructor(props) {
		super(props);
		this.toggleForm = this.toggleForm.bind(this);
		this.reload = this.reload.bind(this);
		this.toggleFormAdd = this.toggleFormAdd.bind(this);
		this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
		this.state = {
			selected: false,
			collapseForm: false,
			tableData: [],
			isLoaded: false,
			errorStatus: 0,
			error: null,
			indexRowSelected: null,
			userSelected: null,
			addAction: false,
			updateAction: false,
		};
		this.rowSelected = this.rowSelected.bind(this);
		this.deleteCaregiver = this.deleteCaregiver.bind(this);
	}

	componentDidMount() {
		this.fetchCaregivers();
	}

	fetchCaregivers() {
		return API_USERS.getCaregivers((result, status, err) => {
			if (result !== null && status === 200) {
				this.setState({
					tableData: result,
					isLoaded: true,
				});
			} else {
				this.setState({
					errorStatus: status,
					error: err,
				});
			}
		});
	}

	toggleForm() {
		this.setState({ selected: !this.state.selected });
	}
	toggleFormAdd(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: true, updateAction: false});
	}
	toggleFormUpdate(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: false, updateAction: true});

	}

	reload() {
		this.setState({
			isLoaded: false,
			addACtion: false,
			updateAction: false,
			selected: false,
		});
		this.fetchCaregivers();
	}

	rowSelected(index, rowInfo){
		this.setState({indexRowSelected:index, userSelected:rowInfo});
	}

	deleteCaregiver(){
		if(this.state.userSelected !== null){
			API_USERS.deleteCaregiver(this.state.userSelected.id, (result, status, err) => {
				if (result !== null && status === 200) {
					this.reload();
				} else {
					this.setState({
						errorStatus: status,
						error: err,
					});
				}
			});
		}
	}

	render() {
		return (
			<div>
				<CardHeader>
					<strong> Caregiver Management </strong>
				</CardHeader>
				<Card style={{ height: '50rem' }}>
					<br />
					<Row>
						<Col sm={{ size: "8", offset: 1 }}>
							<Button color="primary" onClick={this.toggleFormAdd}>
								Add Caregiver{" "}
							</Button>
							<Button color="primary" onClick={this.toggleFormUpdate}>
								Update Caregiver{" "}
							</Button>
							<Button color="primary" onClick={this.deleteCaregiver}>
								Delete Caregiver{" "}
							</Button>
						</Col>
					</Row>
					<br />
					<Row>
						<Col sm={{ size: "10", offset: 1 }}>
							{this.state.isLoaded && (
								<CaregiverTable tableData={this.state.tableData} rowSelected={this.rowSelected}/>
							)}
							{this.state.errorStatus > 0 && (
								<APIResponseErrorMessage
									errorStatus={this.state.errorStatus}
									error={this.state.error}
								/>
							)}
						</Col>
					</Row>
				</Card>

				{
					<Modal
						isOpen={this.state.selected}
						toggle={this.toggleForm}
						className={this.props.className}
						size="lg"
					>
						<ModalHeader toggle={this.toggleForm}> {this.state.addAction ? "Add Caregiver" : "Update Caregiver"} </ModalHeader>
						<ModalBody>
							{this.state.addAction &&(
								<CaregiverForm reloadHandler={this.reload} user={null}/>
							)}
							{this.state.updateAction &&(
								<CaregiverFormUpdate reloadHandler={this.reload} user={this.state.userSelected} />
							)}
						</ModalBody>
					</Modal>
				}
			</div>
		);
	}
}

export default CaregiverContainer;
