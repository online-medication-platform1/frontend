import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavigationBar from "./navigation-bar";
import Home from "./home/home";
import Login from "./authentication/signin";
import DoctorHome from "./home/home_doctor";
import PatientContainer from "./person/patient-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import MedicationContainer from "./medication/medication_container";
import PatientHome from "./home/patient_dashboard/home_patient";
import CaregiverHome from "./home/caregiver_dashboard/home_caregiver";

import ErrorPage from "./commons/errorhandling/error-page";
import styles from "./commons/styles/project-style.css";

class App extends React.Component {
	constructor() {
		super();

		this.state = {
			logginStatus: false,
			user: null,
		};

		this.handleLogin = this.handleLogin.bind(this);
		this.user = localStorage.getItem("login") ? JSON.parse(localStorage.getItem("login")).user : null;
	}

	handleLogin(result) {
		this.setState({
			logginStatus: true,
			user: result,
		});
	}

	render() {
		return (
			<div className={styles.back}>
				<Router>
					<div>
						<NavigationBar user={this.user}/>
						<Switch>
							<Route exact path="/" render={(props) => <Home {...props} />} />

							<Route exact path="/person" render={() => <PatientContainer />} />
							<Route exact path="/caregiver" render={() => <CaregiverContainer />} />
							<Route exact path="/medication" render={() => <MedicationContainer />} />

							<Route
								exact
								path="/signin"
								render={(props) => (
									<Login {...props} handleLogin={this.handleLogin} />
								)}
							/>

							<Route
								exact
								path="/doctor_dashboard"
								render={(props) => <DoctorHome {...props} />}
							/>
							
							<Route
								exact
								path="/patient_dashboard"
								render={(props) => <PatientHome {...props} />}
							/>

							<Route
								exact
								path="/caregiver_dashboard"
								render={(props) => <CaregiverHome {...props} />}
							/>

							{/*Error*/}
							<Route exact path="/error" render={() => <ErrorPage />} />

							<Route render={() => <ErrorPage />} />
						</Switch>
					</div>
				</Router>
			</div>
		);
	}
}

export default App;
