import React, { useReducer } from "react";
import logo from "./commons/images/icon.png";

import {
	DropdownItem,
	DropdownMenu,
	DropdownToggle,
	Nav,
	Navbar,
	NavbarBrand,
	NavLink,
	UncontrolledDropdown,
	Form,
	Button,
} from "reactstrap";

const textStyle = {
	color: "white",
	textDecoration: "none",
};

const logout = () => {
	localStorage.removeItem("login");
	validAuth = false;
};

let validAuth = false;
let isDoctor = false;
const isvalid = (props) =>{
	if(props.user !== null)
		validAuth = true;
	if(validAuth && props.user.roles[0] === "ROLE_DOCTOR")
		isDoctor= true;
}

const NavigationBar = (props) => (
	isvalid(props),
	<div>
		<Navbar color="dark" light expand="md">
			<NavbarBrand href="/">
				<img src={logo} width={"50"} height={"35"} />
			</NavbarBrand>
			<Nav className="mr-auto" navbar>
				{isDoctor  ? 
				<UncontrolledDropdown nav inNavbar>
					<DropdownToggle style={textStyle} nav caret>
						Menu
					</DropdownToggle>
					<DropdownMenu right>
						<DropdownItem>
							<NavLink href="/person">Patients</NavLink>
							<NavLink href="/caregiver">Caregivers</NavLink>
							<NavLink href="/medication">Medications</NavLink>
						</DropdownItem>
					</DropdownMenu>
				</UncontrolledDropdown>
				: null} 
			</Nav>
			
			<Form inline>
			{validAuth ? <Button disabled> Authenticated as: {props.user.name} </Button> : <Button href="/signin">Signin</Button>  }
				<Button onClick={logout} href="/">Logout</Button>
			</Form>
		</Navbar>
	</div>
);

export default NavigationBar;
