import React from "react";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
	Button,
	Card,
	CardHeader,
	Col,
	Modal,
	ModalBody,
	ModalHeader,
	Row,
} from "reactstrap";
import PatientForm from "./components/patient-form";
import PatientFormUpdate from "./components/patient-form-update";
import PatientFormAddMedicationPlan from "./components/patient_form_add_medication_plan";

import * as API_USERS from "./api/patient-api";
import PatientTable from "./components/patient-table";

class PatientContainer extends React.Component {
	constructor(props) {
		super(props);
		this.toggleForm = this.toggleForm.bind(this);
		this.toggleFormAdd = this.toggleFormAdd.bind(this);
		this.toggleFormAddMedicationPlan = this.toggleFormAddMedicationPlan.bind(this);
		this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
		this.reload = this.reload.bind(this);
		this.state = {
			selected: false,
			collapseForm: false,
			tableData: [],
			isLoaded: false,
			errorStatus: 0,
			error: null,
			indexRowSelected: null,
			userSelected: null,
			addAction: false,
			updateAction: false,
			addMedicationPlan: false,
		};
		this.rowSelected = this.rowSelected.bind(this);
		this.deletePatient = this.deletePatient.bind(this);
	}

	componentDidMount() {
		this.fetchPatients();
	}

	fetchPatients() {
		return API_USERS.getPatients((result, status, err) => {
			if (result !== null && status === 200) {
				this.setState({
					tableData: result,
					isLoaded: true,
				});
			} else {
				this.setState({
					errorStatus: status,
					error: err,
				});
			}
		});
	}

	toggleForm() {
		this.setState({ selected: !this.state.selected});
	}
	toggleFormAdd(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: true, updateAction: false, addMedicationPlan: false});
	}
	toggleFormUpdate(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: false, updateAction: true, addMedicationPlan: false});

	}
	toggleFormAddMedicationPlan(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: false, updateAction: false, addMedicationPlan: true});

	}

	deletePatient(){
		if(this.state.userSelected !== null){
			API_USERS.deletePatient(this.state.userSelected.id, (result, status, err) => {
				if (result !== null && status === 200) {
					this.reload();
				} else {
					this.setState({
						errorStatus: status,
						error: err,
					});
				}
			});
		}
	}


	reload() {
		this.setState({
			isLoaded: false,
			addACtion: false,
			addMedicationPlan: false,
			updateAction: false,
			selected: false,
		});
		this.fetchPatients();
	}

	rowSelected(index, rowInfo){
		this.setState({indexRowSelected:index, userSelected:rowInfo});
	}

	render() {
		return (
			<div>
				<CardHeader>
					<strong> Patient Management </strong>
				</CardHeader>
				<Card style={{ height: '50rem' }}>
					<br />
					<Row>
						<Col sm={{ size: "8", offset: 1 }}>
							<Button color="primary" onClick={this.toggleFormAdd}>
								Add Patient{" "}
							</Button>
							<Button color="primary" onClick={this.toggleFormUpdate}>
								Update Patient{" "}
							</Button>
							<Button color="primary" onClick={this.deletePatient}>
								Delete Patient{" "}
							</Button>
							<Button color="primary" onClick={this.toggleFormAddMedicationPlan}>
								Add medication Plan{" "}
							</Button>
						</Col>
					</Row>
					<br />
					<Row>
						<Col sm={{ size: "10", offset: 1 }}>
							{this.state.isLoaded && (
								<PatientTable tableData={this.state.tableData} rowSelected={this.rowSelected} />
							)}
							{this.state.errorStatus > 0 && (
								<APIResponseErrorMessage
									errorStatus={this.state.errorStatus}
									error={this.state.error}
								/>
							)}
						</Col>
					</Row>
				</Card>

				{
					<Modal
						isOpen={this.state.selected}
						toggle={this.toggleForm}
						className={this.props.className}
						size="lg"
					>
						<ModalHeader toggle={this.toggleForm}> {this.state.addAction ? "Add patient" : "Update Patient"} </ModalHeader>
						<ModalBody>
							{this.state.addAction &&(
								<PatientForm reloadHandler={this.reload} user={null}/>
							)}
							{this.state.updateAction &&(
								<PatientFormUpdate reloadHandler={this.reload} user={this.state.userSelected} />
							)}
							{this.state.addMedicationPlan &&(
								<PatientFormAddMedicationPlan reloadHandler={this.reload} user={this.state.userSelected} />
							)}
							 
						</ModalBody>
					</Modal>
				}
			</div>
		);
	}
}

export default PatientContainer;
