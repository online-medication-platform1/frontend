import React from "react";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/patient-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Col, Row } from "reactstrap";
import { FormGroup, Input, Label } from "reactstrap";

class PatientFormAddMedicationPlan extends React.Component {
	constructor(props) {
		super(props);
		this.reloadHandler = this.props.reloadHandler;
		this.user = this.props.user;

		this.state = {
			errorStatus: 0,
			error: null,

			formIsValid: false,

			medicationPlanData: {
                medicationIdList: "",
                intakeIntervalList: "",
                perioadTreatment: "",
				patientId: ""
            },

        };
        this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	postMedicationPlan(MedicationPlan) {
		return API_USERS.postMedicationPlan(MedicationPlan, (result, status, error) => {
			if (result !== null && (status === 200 || status === 201)) {
				console.log("Successfully insert MedicationPlan with id: " + result);
				this.reloadHandler();
			} else {
				this.setState({
					errorStatus: status,
					error: error,
				});
			}
		});
    }
    
    handleChange = (event) => {
		const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.medicationPlanData;
        updatedControls[name] = value;
    }

	handleSubmit() {
		let medicationIds = this.state.medicationPlanData.medicationIdList.split(",");
		let intakeIntervals = this.state.medicationPlanData.intakeIntervalList.split(",");
		console.log(intakeIntervals);
		console.log(medicationIds);

		let MedicationPlan = {
			medicationIdList: medicationIds,
			intakeIntervalList: intakeIntervals,
			perioadTreatment: this.state.medicationPlanData.perioadTreatment,
            patientId: this.user.id,
		};


		console.log(MedicationPlan);
		this.postMedicationPlan(MedicationPlan);
	}

	render() {
		return (
			<div>
				<FormGroup id="medicationIdList">
					<Label for="medicationIdListField"> Medication Id List: </Label>
					<Input
						name="medicationIdList"
						id="medicationIdListField"
                        defaultValue=""
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="intakeIntervalList">
					<Label for="intakeIntervalListField"> Intake Interval: </Label>
					<Input
						name="intakeIntervalList"
						id="intakeIntervalListField"
                        defaultValue=""
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="perioadTreatment">
					<Label for="perioadTreatmentField"> Perioad Treatment: </Label>
					<Input
						name="perioadTreatment"
						id="perioadTreatmentField"
                        defaultValue=""
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<Row>
					<Col sm={{ size: "4", offset: 8 }}>
						<Button
							type="Add medication plan"
							onClick={this.handleSubmit}
						>
							{" "}
							Submit{" "}
						</Button>
					</Col>
				</Row>

				{this.state.errorStatus > 0 && (
					<APIResponseErrorMessage
						errorStatus={this.state.errorStatus}
						error={this.state.error}
					/>
				)}
			</div>
		);
	}
}

export default PatientFormAddMedicationPlan;
