import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
	patients: "/doctor/patients",
	insert_patient: "/doctor/insert_patient",
	update_patient: "/doctor/update_patient",
	delete_patient: "/doctor/remove_patient",
	insert_medication_plan: "/doctor/add_medication_plan_to_patient"
};

const accessToken = localStorage.getItem("login") ? 
	JSON.parse(localStorage.getItem("login")).user.type +
	" " +
	JSON.parse(localStorage.getItem("login")).user.token : null;


function getPatients(callback) {
	let request = new Request(HOST.backend_api + endpoint.patients, {
		method: "GET",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
	});
	console.log(request.url);
	RestApiClient.performRequest(request, callback);
}

function postPatient(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.insert_patient, {
		method: "POST",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function updatePatient(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.update_patient, {
		method: "PUT",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function deletePatient(id, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.delete_patient, {
		method: "DELETE",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(id),
	});

	RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(medicationPlan, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.insert_medication_plan, {
		method: "POST",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(medicationPlan),
	});

	RestApiClient.performRequest(request, callback);
}

export { getPatients, postPatient, updatePatient, deletePatient, postMedicationPlan };
