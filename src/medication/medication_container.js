import React from "react";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
	Button,
	Card,
	CardHeader,
	Col,
	Modal,
	ModalBody,
	ModalHeader,
	Row,
} from "reactstrap";
import MedicationForm from "./components/medication-form";
import MedicationFormUpdate from "./components/medication-form-update";

import * as API_USERS from "./api/medication-api";
import MedicationTable from "./components/medication-table";

class MedicationContainer extends React.Component {
	constructor(props) {
		super(props);
		this.toggleForm = this.toggleForm.bind(this);
		this.toggleFormAdd = this.toggleFormAdd.bind(this);
		this.toggleFormUpdate = this.toggleFormUpdate.bind(this);
		this.reload = this.reload.bind(this);
		this.state = {
			selected: false,
			collapseForm: false,
			tableData: [],
			isLoaded: false,
			errorStatus: 0,
			error: null,
			indexRowSelected: null,
			medicationSelected: null,
			addAction: false,
			updateAction: false,
		};
		this.rowSelected = this.rowSelected.bind(this);
		this.deleteMedication = this.deleteMedication.bind(this);
	}

	componentDidMount() {
		this.fetchMedications();
	}

	fetchMedications() {
		return API_USERS.getMedications((result, status, err) => {
			if (result !== null && status === 200) {
				this.setState({
					tableData: result,
					isLoaded: true,
				});
			} else {
				this.setState({
					errorStatus: status,
					error: err,
				});
			}
		});
	}

	toggleForm() {
		this.setState({ selected: !this.state.selected });
	}
	toggleFormAdd(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: true, updateAction: false});
	}
	toggleFormUpdate(){
		this.toggleForm();
		this.setState({ selected: !this.state.selected, addAction: false, updateAction: true});

	}

	rowSelected(index, rowInfo){
		this.setState({indexRowSelected:index, medicationSelected:rowInfo});
	}

	deleteMedication(){
		console.log("AICI SE AJUNGE");
		if(this.state.medicationSelected !== null){
			API_USERS.deleteMedication(this.state.medicationSelected.id, (result, status, err) => {
				if (result !== null && status === 200) {
					this.reload();
				} else {
					this.setState({
						errorStatus: status,
						error: err,
					});
				}
			});
		}
	}

	reload() {
		this.setState({
			isLoaded: false,
			addACtion: false,
			updateAction: false,
			selected: false,
		});
		this.fetchMedications();
	}

	render() {
		return (
			<div>
				<CardHeader>
					<strong> Medication Management </strong>
				</CardHeader>
				<Card style={{ height: '50rem' }}>
					<br />
					<Row>
						<Col sm={{ size: "8", offset: 1 }}>
							<Button color="primary" onClick={this.toggleFormAdd}>
								Add Medication{" "}
							</Button>
							<Button color="primary" onClick={this.toggleFormUpdate}>
								Update Medication{" "}
							</Button>
							<Button color="primary" onClick={this.deleteMedication}>
								Delete Medication{" "}
							</Button>
						</Col>
					</Row>
					<br />
					<Row>
						<Col sm={{ size: "10", offset: 1 }}>
							{this.state.isLoaded && (
								<MedicationTable tableData={this.state.tableData} rowSelected={this.rowSelected}/>
							)}
							{this.state.errorStatus > 0 && (
								<APIResponseErrorMessage
									errorStatus={this.state.errorStatus}
									error={this.state.error}
								/>
							)}
						</Col>
					</Row>
				</Card>

				{
					<Modal
						isOpen={this.state.selected}
						toggle={this.toggleForm}
						className={this.props.className}
						size="lg"
					>
						<ModalHeader toggle={this.toggleForm}>{this.state.addAction ? "Add medication" : "Update Medication"}  </ModalHeader>
						<ModalBody>
							{this.state.addAction &&(
								<MedicationForm reloadHandler={this.reload} medication={null}/>
							)}
							{this.state.updateAction &&(
								<MedicationFormUpdate reloadHandler={this.reload} medication={this.state.medicationSelected} />
							)}
						</ModalBody>
					</Modal>
				}
			</div>
		);
	}
}

export default MedicationContainer;
