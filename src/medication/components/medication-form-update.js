import React from "react";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Col, Row } from "reactstrap";
import { FormGroup, Input, Label } from "reactstrap";

class MedicationFormUpdate extends React.Component {
	constructor(props) {
		super(props);
		this.reloadHandler = this.props.reloadHandler;
		this.medication = this.props.medication;
		if(this.medication === null)
			this.medication = {name: "", sideEffects: "", dosage: ""}

		this.state = {
			errorStatus: 0,
			error: null,

			formIsValid: false,

            medicationData: {
                name: this.medication.name,
                sideEffects: this.medication.sideEffects,
                dosage: this.medication.dosage,
            },
        };
        this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	updateMedication(Medication) {
		return API_USERS.updateMedication(Medication, (result, status, error) => {
			if (result !== null && (status === 200 || status === 201)) {
				console.log("Successfully update Medication with id: " + result);
				this.reloadHandler();
			} else {
				this.setState({
					errorStatus: status,
					error: error,
				});
			}
		});
    }
    
    handleChange = (event) => {
		const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.medicationData;
        updatedControls[name] = value;
    }

	handleSubmit() {
		let Medication = {
            id: this.medication.id,
			name: this.state.medicationData.name,
			sideEffects: [this.state.medicationData.sideEffects],
			dosage: this.state.medicationData.dosage,
		};

		console.log(Medication);
		this.updateMedication(Medication);
	}

	render() {
		return (
			<div>
				<FormGroup id="name">
					<Label for="nameField"> Name: </Label>
					<Input
						name="name"
						id="nameField"
                        defaultValue={this.state.medicationData.name}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="sideEffects">
					<Label for="sideEffectsField"> SideEffects: </Label>
					<Input
						name="sideEffects"
						id="sideEffectsField"
                        defaultValue={this.state.medicationData.sideEffects}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<FormGroup id="dosage">
					<Label for="dosageField"> Dosage: </Label>
					<Input
						name="dosage"
						id="dosageField"
                        defaultValue={this.state.medicationData.dosage}
                        onChange={this.handleChange}
					/>
				</FormGroup>

				<Row>
					<Col sm={{ size: "4", offset: 8 }}>
						<Button
							type={this.medication.name==="" ? "Insert" : "Update"}
							onClick={this.handleSubmit}
						>
							{" "}
							Submit{" "}
						</Button>
					</Col>
				</Row>

				{this.state.errorStatus > 0 && (
					<APIResponseErrorMessage
						errorStatus={this.state.errorStatus}
						error={this.state.error}
					/>
				)}
			</div>
		);
	}
}

export default MedicationFormUpdate;
