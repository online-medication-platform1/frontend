import React from "react";
import Table from "../../commons/tables/table";

const columns = [
	{
		Header: "ID",
		accessor: "id",
	},
	{
		Header: "Name",
		accessor: "name",
	},
	{
		Header: "SideEffects",
		accessor: "listOfSideEffects",
	},
	{
		Header: "Dosage",
		accessor: "dosage",
	},
];

const filters = [
	{
		accessor: "name",
	},
];

class MedicationTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			tableData: this.props.tableData,
		};
	}

	render() {
		return (
			<Table
				data={this.state.tableData}
				columns={columns}
				search={filters}
				pageSize={10} 
				rowSelected={this.props.rowSelected}
			/>
		);
	}
}

export default MedicationTable;
