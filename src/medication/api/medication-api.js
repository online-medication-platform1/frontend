import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
	medications: "/doctor/medications",
	insert_medication: "/doctor/insert_medication",
	update_medication: "/doctor/update_medication",
	remove_medication: "/doctor/remove_medication",
};

const accessToken = localStorage.getItem("login") ? 
	JSON.parse(localStorage.getItem("login")).user.type +
	" " +
	JSON.parse(localStorage.getItem("login")).user.token : null;

function getMedications(callback) {
	let request = new Request(HOST.backend_api + endpoint.medications, {
		method: "GET",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
	});
	console.log(request.url);
	RestApiClient.performRequest(request, callback);
}

function postMedication(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.insert_medication, {
		method: "POST",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function updateMedication(user, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.update_medication, {
		method: "PUT",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(user),
	});

	RestApiClient.performRequest(request, callback);
}

function deleteMedication(id, callback) {
	console.log(accessToken);
	let request = new Request(HOST.backend_api + endpoint.remove_medication, {
		method: "DELETE",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": accessToken
		},
		body: JSON.stringify(id),
	});

	RestApiClient.performRequest(request, callback);
}

export { getMedications, postMedication, updateMedication, deleteMedication };
