import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
	person: "/auth/signin",
};

function postLogin(user, callback) {
	let request = new Request(HOST.backend_api + endpoint.person, {
		method: "POST",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json",
		},
		body: JSON.stringify(user),
	});

	console.log("URL: " + request.url);

	RestApiClient.performRequest(request, callback);
}

export { postLogin };
