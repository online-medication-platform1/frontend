import React from "react";
import { Button } from "react-bootstrap";
import { Form, Container, Row, Col } from "react-bootstrap";
import validate from "../person/components/validators/patient-validators";
import * as API_LOGIN from "./api/auth-api";
import { Redirect } from "react-router-dom";

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.toggleForm = this.toggleForm.bind(this);
		this.reloadHandler = this.props.reloadHandler;

		this.state = {
			errorStatus: 0,
			error: null,

			formIsValid: false,

			formControls: {
				email: {
					value: "",
					placeholder: "Enter email",
					valid: false,
					touched: false,
					validationRules: {
						emailValidator: true,
					},
				},
				password: {
					value: "",
					placeholder: "Password",
					valid: false,
					touched: false,
				},
			},
			response: {
				id: "",
				email: "",
				roles: [],
				accessToken: "",
				tokenType: "",
			},
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
	}

	handleChange = (event) => {
		const name = event.target.name;
		const value = event.target.value;

		const updatedControls = this.state.formControls;

		const updatedFormElement = updatedControls[name];

		updatedFormElement.value = value;
		updatedFormElement.touched = true;
		updatedFormElement.valid = validate(
			value,
			updatedFormElement.validationRules
		);
		updatedControls[name] = updatedFormElement;

		let formIsValid = true;
		for (let updatedFormElementName in updatedControls) {
			formIsValid =
				updatedControls[updatedFormElementName].valid && formIsValid;
		}

		this.setState({
			formControls: updatedControls,
			formIsValid: formIsValid,
		});
	};

	toggleForm() {
		this.setState({ collapseForm: !this.state.collapseForm });
	}

	login(user) {
		API_LOGIN.postLogin(user, (result, status, error) => {
			if (result !== null && (status === 200 || status === 201)) {
				//console.log(result.accessToken);
				this.setState({ response: result });
				//console.log(this.state.response);
				localStorage.setItem(
					"login",
					JSON.stringify({
						login: true,
						user: this.state.response,
					})
				);
				this.handleSuccessfulAuth(result);
			} else {
				this.setState({
					errorStatus: status,
					error: error,
				});
			}
		});
	}

	handleSubmit() {
		let user = {
			email: this.state.formControls.email.value,
			password: this.state.formControls.password.value,
		};
		this.login(user);
	}

	handleSuccessfulAuth(result) {
		this.props.handleLogin(result);
		/* if(this.state.response.roles[0] === "ROLE_DOCTOR")
			this.props.history.push("/doctor_dashboard");
		if(this.state.response.roles[0] === "ROLE_CAREGIVER")
			this.props.history.push("/caregiver_dashboard");
		if(this.state.response.roles[0] === "ROLE_PATIENT")
			this.props.history.push("/patient_dashboard"); */
		switch(this.state.response.roles[0]){
			case "ROLE_DOCTOR":
				this.props.history.push("/doctor_dashboard");
				break;
			case "ROLE_CAREGIVER":
				this.props.history.push("/caregiver_dashboard");
				break;
			case "ROLE_PATIENT":
				this.props.history.push("/patient_dashboard");
				break;
			default:
				this.props.history.push("/signin");

		}
	}

	render() {
		return (
			<div>
				<Container>
					<Row>
						<Col md={{ span: 12, offset: 5 }}>
							<h2>Login</h2>
						</Col>
					</Row>
					<Row>
						<Col md={{ span: 6, offset: 3 }}>
							<Form>
								<Form.Group id="email">
									<Form.Label>Email address</Form.Label>
									<Form.Control
										id="emailField"
										name="email"
										type="email"
										placeholder={this.state.formControls.email.placeholder}
										onChange={this.handleChange}
										defaultValue={this.state.formControls.email.value}
										touched={this.state.formControls.email.touched ? 1 : 0}
										valid={this.state.formControls.email.validationRules}
										required
									/>
								</Form.Group>

								<Form.Group id="password">
									<Form.Label>Password</Form.Label>
									<Form.Control
										id="passwordField"
										name="password"
										type="password"
										placeholder={this.state.formControls.password.placeholder}
										onChange={this.handleChange}
										defaultValue={this.state.formControls.password.value}
										touched={this.state.formControls.password.touched ? 1 : 0}
										required
									/>
								</Form.Group>
								<Row>
									<Col md={{ span: 3, offset: 1 }}>
										<Button
											variant="primary"
											onClick={this.handleSubmit}
											disabled={!this.state.formIsValid}
										>
											Login
										</Button>
									</Col>
									<Col md={{ span: 3, offset: 4 }}>
										<Button variant="primary">Register</Button>
									</Col>
								</Row>
							</Form>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Login;
